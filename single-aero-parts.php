<?php get_header(); ?>
<div class="content"><!-- Contents -->

  <section class="post-aero">

    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>

        <div class="post-aero-container">
          <h2>Aero Parts</h2>
          <h3><?php the_title(); ?></h3>

          <div class="aero-maker"><div><?php echo get_post_meta($post->ID , 'car_maker' ,true); ?></div></div>

          <div class="aero-box clearfix">

            <?php if(check_new_post( get_post_time('Y-m-d') )): ?>
              <div class="aero-box-thum pickup-img">
            <?php else: ?>
               <div class="aero-box-thum">
            <?php endif ?>

            <?php if(has_post_thumbnail()): ?>
              <?php the_post_thumbnail(array( 200, 170 )); ?>
            <?php else: ?>
              <img src="<?php bloginfo('template_directory'); ?>/img/noimage.png" width="200" height="170" alt="noimage"/>
            <?php endif ?>
            </div>

            <div class="aero-box-info">
              <div class="aero-box-type">
                <p class="aero-box-label">車種：</p>
                <h3><?php echo get_post_meta($post->ID , 'car_type1' ,true); ?></h3>
                <?php echo get_post_meta($post->ID , 'car_type2' ,true); ?>
              </div>

              <div class="aero-box-num">
                型番：<?php echo get_post_meta($post->ID , 'car_type_num' ,true); ?>
              </div>

              <div class="aero-box-cat">
                <?php echo get_the_term_list($post->ID, 'aero-cat','',' '); ?>
              </div>
            </div>

          </div>

          <div class="post-box-content">
            <?php the_content(); ?>
          </div>
        </div>

      <?php endwhile; ?>

    <?php else : ?>
      <h3>ページが見つかりませんでした。</h3>
    <?php endif; ?>

  </section>
</div><!-- content -->

<?php get_footer(); ?>
