<?php 

get_header(); ?>
<div class="content">
<!-- Contents -->
  <section class="news-list">

    <div class="info-list-archive-container">
      <h2>News</h2>
      <h3><?php single_term_title(); ?></h3>

      <?php while (have_posts()) : the_post(); ?>

          <div class="infomation_box">

            <div class="infomation_box_time">
              <p><?php the_time('Y年n月j日'); ?></p>
            </div>

            <?php if(check_new_post( get_post_time('Y-m-d') )): ?>
              <div class="infomation_box_thum pickup-img">
            <?php else: ?>
              <div class="infomation_box_thum">
            <?php endif ?>

            <?php echo '<a href="'. get_permalink() .'">'; ?>
              <?php if( has_post_thumbnail()): ?>
                <?php the_post_thumbnail( array( 200, 170 )); ?>
              <?php else: ?>
                <img src="<?php bloginfo('template_url'); ?>/img/logo_thum.png" width="200" height="170" alt="no image"/>
              <?php endif; ?>
            </a>
            </div>

            <div class="infomation_box_title">
              <?php echo '<a href="'. get_permalink() .'">'. the_short_title(40) . '</a>'; ?>
            </div>

            <div class="infomation_box_cat">
              <?php echo get_the_term_list($post->ID, 'info-msg-cat','',' '); ?>
            </div>

          </div>
      <?php endwhile; ?>

<?php
$args = array (
    'next_text'          => 'NEXT&gt;',
    'prev_text'          => '&lt;PREV',
);
the_posts_pagination($args);
wp_reset_postdata();
?>

    </div>
  </section>
</div>
<!-- content -->



<?php get_footer(); ?>