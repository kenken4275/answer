<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <!-- External files -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" />
  <?php wp_head(); ?>
</head>
<body>

  <div id="page">

    <div id="loader-bg">
      <div id="loader">
        <img src="<?php bloginfo('template_url'); ?>/img/img-loading.gif" width="80" height="80" alt="Now Loading..." />
        <p>Now Loading...</p>
      </div>
    </div>

    <div id="wrapper">

      <!-- Header -->
      <div id="header">
        <div id="nav_box_wrapper">
          <div id="nav_box">

            <div id="title_top">
              <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            </div>

            <div id="description_top">
              <?php echo get_bloginfo( 'description', 'display' ); ?>
            </div>

            <div id="tel_top">
              <?php if ( wp_is_mobile() ) : ?>
                <i class="fas fa-phone-volume"></i>
                <a id="tel_num" href="tel:<?php echo get_option( 'answer_tel' ); ?>"><?php echo get_option( 'answer_tel' ); ?></a>
              <?php else: ?>
                <i class="fas fa-phone-volume"></i>
                <?php echo get_option( 'answer_tel' ); ?>
              <?php endif; ?>
            </div>

            <div id="toggle"><a href="#"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a></div>

            <?php
              wp_nav_menu(array(
                  'theme_location' => 'nav-bar',
                  'menu_id' =>'menu'
              ));
            ?>
          </div>
        </div>

        <!-- custom header -->
        <?php if(! is_front_page()): ?>
          <?php $header_image = get_header_image(); ?>
          <?php if ( ! empty( $header_image ) ): ?>
            <div id="header_img">
              <a href="<?php echo home_url(); ?>">
                <img id="head_bk_img" src="<?php header_image(); ?>" alt="<?php bloginfo('name'); ?>"  />
                <?php if ( wp_is_mobile() ) : ?>
                  <p><img id="head_logo" src="<?php bloginfo('template_url'); ?>/img/logo_mini.png"/></p>
                <?php else: ?>
                  <p><img id="head_logo" src="<?php bloginfo('template_url'); ?>/img/logo.png"/></p>
                <?php endif; ?>
              </a>
            </div>
          <?php else: ?>
            <div id="header_img">
              <a href="<?php echo home_url(); ?>">
                <img id="head_logo" src="<?php bloginfo('template_url'); ?>/img/hd.png"/>
                <?php if ( wp_is_mobile() ) : ?>
                  <p><img id="head_logo" src="<?php bloginfo('template_url'); ?>/img/logo_mini.png"/></p>
                <?php else: ?>
                  <p><img id="head_logo" src="<?php bloginfo('template_url'); ?>/img/logo.png"/></p>
                <?php endif; ?>
              </a>
            </div>
          <?php endif;?>
        <?php endif; ?>

        <?php if(! is_front_page()): ?>
          <div id="bd_wrapper">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
              <?php if(function_exists('bcn_display')){ bcn_display(); }?>
            </div>
          </div>
        <?php endif; ?>

      </div><!-- /#header -->
