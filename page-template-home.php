<?php 
/**
 * Template Name: page-template-home
*/

get_header(); ?>
<div class="content">
<!-- Contents -->

  <section class="logo">
    <?php get_template_part( 'sections/logo' ); ?>
  </section>

  <section class="intro">
    <?php get_template_part( 'sections/intro' ); ?>
  </section>

  <section class="news">
    <?php get_template_part( 'sections/news' ); ?>
  </section>

  <section class="outline">
    <?php get_template_part( 'sections/outline' ); ?>
  </section>

  <section class="access">
    <?php get_template_part( 'sections/access' ); ?>
  </section>

  <section class="contact">
    <?php get_template_part( 'sections/contact' ); ?>
  </section>

</div><!-- content -->

<?php get_footer(); ?>