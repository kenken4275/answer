<?php 
/**
 * Template Name: archive-info-msg
*/

get_header(); ?>
<div class="content">
<!-- Contents -->
  <section class="news-list">

    <div class="info-list-archive-container">
      <h2>News</h2>

<?php
$args = array(
  'post_type' => 'info-msg',
  'posts_per_page' => 8,
  'posts_per_archive_page' => 8,
  'orderby' => 'date', //日付でソート
  'order' => 'DESC', //日付が新しい順（降順）で表示
  'paged' => get_query_var( 'paged' ),/* ページネーションする場合は必須 */
);
$the_query = new WP_Query($args);
?>

      <?php if ($the_query->have_posts()) : ?>
        <?php while ($the_query-> have_posts() ) : $the_query->the_post(); ?>

          <div class="infomation_box">

            <div class="infomation_box_time">
              <p><?php the_time('Y年n月j日'); ?></p>
            </div>

            <?php if(check_new_post( get_post_time('Y-m-d') )): ?>
              <div class="infomation_box_thum pickup-img">
            <?php else: ?>
              <div class="infomation_box_thum">
            <?php endif ?>

            <?php echo '<a href="'. get_permalink() .'">'; ?>
              <?php if( has_post_thumbnail()): ?>
                <?php the_post_thumbnail( array( 200, 170 )); ?>
              <?php else: ?>
                <img src="<?php bloginfo('template_url'); ?>/img/logo_thum.png" width="200" height="170" alt="no image"/>
              <?php endif; ?>
            </a>
            </div>

            <div class="infomation_box_title">
              <?php echo '<a href="'. get_permalink() .'">'. the_short_title(40) . '</a>'; ?>
            </div>

            <div class="infomation_box_cat">
              <?php echo get_the_term_list($post->ID, 'info-msg-cat','',' '); ?>
            </div>

          </div>
          

        <?php endwhile; ?>
<?php
//ページネーション表示前に$GLOBALS['wp_query']->max_num_pagesに値をセット
$GLOBALS['wp_query']->max_num_pages = $the_query->max_num_pages;

$args = array (
    'next_text'          => 'NEXT&gt;',
    'prev_text'          => '&lt;PREV',
);
the_posts_pagination($args);
wp_reset_postdata();
?>
      <?php endif ?>

    </div>
  </section>
</div>
<!-- content -->



<?php get_footer(); ?>