﻿jQuery(document).ready(function($){
  //スライダー
  $( '#slider1' ).sliderPro({
      width: '100%',//横幅
      arrows: true,//左右の矢印を出す
      buttons: false,//ナビゲーションボタンを出さない
      aspectRatio: 1.5,//スライドのアスペクト比
      forceSize: 'fullWindow',//ウィンドウ内全画面表示
      slideDistance:0,//スライド同士の距離
      autoplayOnHover:'none',//マウスカーソルをスライダーにホバーさせた時　何もしない
  });

  //ローディング
  $(function() {
    var h = $(window).height();
    $('#wrapper').css('visibility','hidden');
    $('#loader-bg ,#loader').height(h).css('display','block');
  });
  $(window).load(function () { //全ての読み込みが完了したら実行
    $('#loader-bg').delay(900).fadeOut(800);
    $('#loader').delay(600).fadeOut(300, function(){ $('#wrapper').css('visibility', 'visible'); });
  });



  //スマホメニュー
  $("#toggle").click(function(){
    $("#menu").slideToggle();
    return false;
  });
  $(window).resize(function(){
    var win = $(window).width();
    var p = 640;
    if(win > p){
      $("#menu").show();
    }
  });


});
