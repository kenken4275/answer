﻿jQuery(document).ready(function($){
  var custom_uploader;

  $('#media-upload_1').click(function(e) {
    e.preventDefault();
    if (custom_uploader) {
      custom_uploader.open();
      return;
    } 
    custom_uploader = wp.media({
      title: 'Choose Image',
      library: {
        type: 'image'
      }, 
      button: {
        text: 'Choose Image'
      },  
      multiple: false // 画像を1つ選択
    }); 
    custom_uploader.open();

    custom_uploader.on("select", function () {
      var images = custom_uploader.state().get('selection');
      images.each(function(file) {
        $("#answer_slider_img_id_1").val(file.toJSON().id);
        $("#image-view_1").attr("src", file.toJSON().url);
      });
    });
  });
});

jQuery(document).ready(function($){
  var custom_uploader;

  $('#media-upload_2').click(function(e) {
    e.preventDefault();
    if (custom_uploader) {
      custom_uploader.open();
      return;
    } 
    custom_uploader = wp.media({
      title: 'Choose Image',
      library: {
        type: 'image'
      }, 
      button: {
        text: 'Choose Image'
      },  
      multiple: false // 画像を1つ選択
    }); 
    custom_uploader.open();

    custom_uploader.on("select", function () {
      var images = custom_uploader.state().get('selection');
      images.each(function(file) {
        $("#answer_slider_img_id_2").val(file.toJSON().id);
        $("#image-view_2").attr("src", file.toJSON().url);
      });
    });
  });
});

jQuery(document).ready(function($){
  var custom_uploader;

  $('#media-upload_3').click(function(e) {
    e.preventDefault();
    if (custom_uploader) {
      custom_uploader.open();
      return;
    } 
    custom_uploader = wp.media({
      title: 'Choose Image',
      library: {
        type: 'image'
      }, 
      button: {
        text: 'Choose Image'
      },  
      multiple: false // 画像を1つ選択
    }); 
    custom_uploader.open();

    custom_uploader.on("select", function () {
      var images = custom_uploader.state().get('selection');
      images.each(function(file) {
        $("#answer_slider_img_id_3").val(file.toJSON().id);
        $("#image-view_3").attr("src", file.toJSON().url);
      });
    });
  });
});
