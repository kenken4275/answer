<?php get_header(); ?>
<div class="content"><!-- Contents -->

  <section class="post-accessory">

    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>

        <div class="post-accessory-container">
          <h2>Accessory</h2>
          <h3><?php the_title(); ?></h3>

          <div class="accessory-num"><div><?php echo get_post_meta($post->ID , 'accessory_num' ,true); ?></div></div>

          <div class="accessory-box clearfix">

            <?php if(check_new_post( get_post_time('Y-m-d') )): ?>
              <div class="accessory-box-thum pickup-img">
            <?php else: ?>
               <div class="accessory-box-thum">
            <?php endif ?>

            <?php if(has_post_thumbnail()): ?>
              <?php the_post_thumbnail(array( 200, 170 )); ?>
            <?php else: ?>
              <img src="<?php bloginfo('template_directory'); ?>/img/noimage.png" width="200" height="170" alt="noimage"/>
            <?php endif ?>
            </div>

            <div class="accessory-box-info">
              <div class="accessory-box-name">
                <h3><?php echo get_post_meta($post->ID , 'accessory_name' ,true); ?></h3>
              </div>

              <div class="accessory-box-price">
                ￥<?php echo get_post_meta($post->ID , 'accessory_price' ,true); ?>
              </div>

              <div class="accessory-box-cat">
                <?php echo get_the_term_list($post->ID, 'accessory-cat','',' '); ?>
              </div>
            </div>

          </div>

          <div class="post-box-content">
            <?php the_content(); ?>
          </div>
        </div>

      <?php endwhile; ?>

    <?php else : ?>
      <h3>ページが見つかりませんでした。</h3>
    <?php endif; ?>

  </section>
</div><!-- content -->

<?php get_footer(); ?>
