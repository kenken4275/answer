<div class="wrap">
  <h2>アンサー情報管理</h2>
  <form method="post" action="options.php">
  <?php 
    settings_fields( 'answer-settings-group' );
    do_settings_sections( 'answer-settings-group' );
  ?>
  <table>
    <tbody>
      <tr>
        <th scope="row">
          <label for="answer_name">販売業者</label>
        </th>
        <td><input type="text" id="answer_name" class="regular-text" name="answer_name" value="<?php echo get_option('answer_name'); ?>"></td>
      </tr>
      <tr>
        <th scope="row">
          <label for="answer_post">郵便番号</label>
        </th>
        <td><input type="text" id="answer_post" class="regular-text" name="answer_post" value="<?php echo get_option('answer_post'); ?>"></td>
      </tr>
      <tr>
        <th scope="row">
          <label for="answer_addr">所在地</label>
        </th>
        <td><input type="text" id="answer_addr" class="regular-text" name="answer_addr" value="<?php echo get_option('answer_addr'); ?>"></td>
      </tr>
      <tr>
        <th scope="row">
          <label for="answer_master">代表者</label>
        </th>
        <td><input type="text" id="answer_master" class="regular-text" name="answer_master" value="<?php echo get_option('answer_master'); ?>"></td>
      </tr>
      <tr>
        <th scope="row">
          <label for="answer_tel">TEL</label>
        </th>
        <td><input type="text" id="answer_tel" class="regular-text" name="answer_tel" value="<?php echo get_option('answer_tel'); ?>"></td>
      </tr>
      <tr>
        <th scope="row">
          <label for="answer_fax">FAX</label>
        </th>
        <td><input type="text" id="answer_fax" class="regular-text" name="answer_fax" value="<?php echo get_option('answer_fax'); ?>"></td>
      </tr>
      <tr>
        <th scope="row">
          <label for="answer_mail">MAIL</label>
        </th>
        <td><input type="text" id="answer_mail" class="regular-text" name="answer_mail" value="<?php echo get_option('answer_mail'); ?>"></td>
      </tr>
    </tbody>
  </table>
  <?php submit_button(); ?>
  </form>
</div>
