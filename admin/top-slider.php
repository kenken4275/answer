<div class="wrap">
  <h2>スライダー設定</h2>
  <form method="post" action="options.php">
  <?php 
    settings_fields( 'answer-slider-group' );
    do_settings_sections( 'answer-slider-group' );
  ?>
  <table>
    <tbody>
      <tr>
        <th scope="row">
          <label for="answer_slider_img_id_1">1</label>
        </th>
        <td>
          <input type="hidden" id="answer_slider_img_id_1" class="regular-text" name="answer_slider_img_id_1" value="<?php echo get_option('answer_slider_img_id_1'); ?>">

            <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_1') , 'thumbnail'); ?>
            <?php $img_url = get_stylesheet_directory_uri() . "/img/noimage220_165.png";?>
            <?php if( $img_src) : ?>
            <?php $img_url = $img_src[0]; ?>
            <?php endif; ?>

          <img id="image-view_1" src="<?php echo $img_url; ?>" width="200" height="170">
          <button id="media-upload_1">Choose Image</button>
        </td>
      </tr>

      <tr>
        <th scope="row">
          <label for="answer_slider_img_id_2">2</label>
        </th>
        <td>
          <input type="hidden" id="answer_slider_img_id_2" class="regular-text" name="answer_slider_img_id_2" value="<?php echo get_option('answer_slider_img_id_2'); ?>">

            <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_2') , 'thumbnail'); ?>
            <?php $img_url = get_stylesheet_directory_uri() . "/img/noimage220_165.png";?>
            <?php if( $img_src) : ?>
            <?php $img_url = $img_src[0]; ?>
            <?php endif; ?>

          <img id="image-view_2" src="<?php echo $img_url; ?>" width="200" height="170">
          <button id="media-upload_2">Choose Image</button>
        </td>
      </tr>

      <tr>
        <th scope="row">
          <label for="answer_slider_img_id_3">3</label>
        </th>
        <td>
          <input type="hidden" id="answer_slider_img_id_3" class="regular-text" name="answer_slider_img_id_3" value="<?php echo get_option('answer_slider_img_id_3'); ?>">

            <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_3') , 'thumbnail'); ?>
            <?php $img_url = get_stylesheet_directory_uri() . "/img/noimage220_165.png";?>
            <?php if( $img_src) : ?>
            <?php $img_url = $img_src[0]; ?>
            <?php endif; ?>

          <img id="image-view_3" src="<?php echo $img_url; ?>" width="200" height="170">
          <button id="media-upload_3">Choose Image</button>
        </td>
      </tr>

    </tbody>
  </table>
  <?php submit_button(); ?>
  </form>
</div>