<?php 
/**
 * Template Name: archive-cars-accessory
*/

get_header(); ?>
<div class="content">
<!-- Contents -->
  <section class="accessory-list">

    <div class="accessory-list-archive-container">
      <h2>Accessory</h2>

<?php
$args = array(
  'post_type' => 'cars-accessory',
  'posts_per_page' => 8,
  'orderby' => 'date', //日付でソート
  'order' => 'DESC', //日付が新しい順（降順）で表示
  'paged' => get_query_var( 'paged' ),/* ページネーションする場合は必須 */
);
$the_query = new WP_Query($args);
?>

      <?php if ($the_query->have_posts()) : ?>
        <?php while ($the_query-> have_posts() ) : $the_query->the_post(); ?>

          <div class="accessory-list-box">
            <div class="accessory-list-box-num">
              <?php echo get_post_meta($post->ID , 'accessory_num' ,true); ?>
            </div>

            <?php if(check_new_post( get_post_time('Y-m-d') )): ?>
              <div class="accessory-list-box-thum pickup-img">
            <?php else: ?>
              <div class="accessory-list-box-thum">
            <?php endif ?>

              <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                <?php if(has_post_thumbnail()): ?>
                  <?php the_post_thumbnail(array( 200, 170 )); ?>
                <?php else: ?>
                  <img src="<?php bloginfo('template_directory'); ?>/img/noimage.png" width="200" height="170" alt="noimage"/>
                <?php endif ?>
              </a>
            </div>

            <div class="accessory-list-box-name">
              <?php echo get_post_meta($post->ID , 'accessory_name' ,true); ?>
            </div>


            <div class="accessory-list-box-cat">
              <?php echo get_the_term_list($post->ID, 'accessory-cat','',' '); ?>
            </div>

          </div>

        <?php endwhile; ?>
<?php
//ページネーション表示前に$GLOBALS['wp_query']->max_num_pagesに値をセット
$GLOBALS['wp_query']->max_num_pages = $the_query->max_num_pages;

$args = array (
    'next_text'          => 'NEXT&gt;',
    'prev_text'          => '&lt;PREV',
);
the_posts_pagination($args);
wp_reset_postdata();
?>
      <?php endif ?>

    </div>
  </section>
</div>
<!-- content -->



<?php get_footer(); ?>