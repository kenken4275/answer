<?php
function init_mywp() {
  // カスタムヘッダー
  $custom_header_args = array(
      // デフォルトで表示するヘッダー画像(画像のURLを入力)
      'default-image' => get_template_directory_uri() . '/img/hd.jpg',
      // ヘッダー画像の横幅
      'width' => 1000,
      // ヘッダー画像の縦幅
      'height' => 200
  );
  add_theme_support( 'custom-header', $custom_header_args );

  // カスタムメニュー
  register_nav_menus( array(
      'nav-bar' => 'ナビゲーションバー',
      'footer-navbar' => 'フッターナビゲーションバー',
  ) );

  // ウィジェット
  // サイドバーのウィジェット
  register_sidebar( array(
      'name' => __( 'Side Widget' ),
      'id' => 'side-widget',
      'before_widget' => '<li class="widget-container">',
      'after_widget' => '</li>',
      'before_title' => '<h3>',
      'after_title' => '</h3>',
  ) );

  // フッターエリアのウィジェット
  register_sidebar( array(
      'name' => __( 'Footer Widget' ),
      'id' => 'footer-widget',
      'before_widget' => '<div class="widget-area"><ul><li class="widget-container">',
      'after_widget' => '</li></ul></div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>',
  ) );

  // アイキャッチ画像利用
  add_theme_support('post-thumbnails');
  // アイキャッチ画像サイズ設定
  set_post_thumbnail_size(200,170, true ); // 幅 高さ 切り抜きモード
  //size1サイズ設定
  add_image_size('size1',300,300);


  //カスタム投稿タイプ「お知らせ」
  register_post_type( 'info-msg', array(
      'label' => 'お知らせ', //管理画面に表示される名前
      'public' => true, //管理画面やサイトにカスタム投稿タイプを表示可能にする設定
      'supports' => array( 'thumbnail', 'title', 'editor', 'author' ),//管理画面に表示する項目
      'menu_position' => 5, //左メニューでの表示位置 5は「投稿」の下
      'has_archive' => 'true' //アーカイブページを持つ
  ));

  //カスタム投稿タイプ「エアロパーツ」
  register_post_type( 'aero-parts', array(
      'label' => 'エアロパーツ',
      'public' => true, 
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields' ),
      'menu_position' => 5, 
      'has_archive' => 'true' 
  ));

  //カスタム投稿タイプ「アクセサリー」
  register_post_type( 'cars-accessory', array(
      'label' => 'アクセサリー',
      'public' => true,
      'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields' ),
      'menu_position' => 5,
      'has_archive' => 'true'
  ));


  //info-msg-cat（お知らせカテゴリー）というカスタム分類を登録
  register_taxonomy(
      'info-msg-cat',  //カスタム分類名
      'info-msg',  //カスタム分類を利用する投稿タイプ
      array(
          'label' =>  'お知らせカテゴリー',  //管理画面ラベル名
          'labels' => array(
              'popular_items' =>  'よく使うお知らせカテゴリー',
              'edit_item' => 'お知らせカテゴリーを編集',
              'add_new_item' => '新規お知らせカテゴリーを追加',
              'search_items' =>  'お知らせカテゴリーを検索',
          ),
      'public' => true,
      'hierarchical' => false  //カテゴリーのような親子関係（階層）を持たせない
      )
  );

  //aero-cat（エアロカテゴリー）というカスタム分類を登録
  register_taxonomy(
      'aero-cat', 
      'aero-parts',
      array(
          'label' =>  'エアロカテゴリー',
          'labels' => array(
              'popular_items' =>  'よく使うエアロカテゴリー',
              'edit_item' => 'エアロカテゴリーを編集',
              'add_new_item' => '新規エアロカテゴリーを追加',
              'search_items' =>  'エアロカテゴリーを検索',
          ),
      'public' => true,
      'hierarchical' => false
      )
  );

  //accessory-cat（アクセサリーカテゴリー）というカスタム分類を登録
  register_taxonomy(
      'accessory-cat', 
      'cars-accessory',
      array(
          'label' =>  'アクセサリーカテゴリー',
          'labels' => array(
              'popular_items' =>  'よく使うアクセサリーカテゴリー',
              'edit_item' => 'アクセサリーカテゴリーを編集',
              'add_new_item' => '新規アクセサリーカテゴリーを追加',
              'search_items' =>  'アクセサリーカテゴリーを検索',
          ),
      'public' => true,
      'hierarchical' => false
      )
  );

}
add_action('init', 'init_mywp');//初期化時


function change_posts_per_page($query) {
  /* 管理画面,メインクエリに干渉しないために必須 */
  if( is_admin() || ! $query->is_main_query() ){
    return;
  }

  if ( $query->is_tax() ) {
    $query->set('posts_per_page', '8' );
    $query->set('paged', get_query_var( 'paged' ));
    return;
  }
}
add_action( 'pre_get_posts', 'change_posts_per_page' );



//オリジナルの設定メニュー追加
function answer_info_menu() {
  //add_options_page('会社情報管理', '会社情報管理', 'administrator', 'answer_info_menu', 'answer_info_options_page');
  add_menu_page('アンサー情報管理', 'アンサー情報管理', 'administrator', 'answer_info_menu', 'answer_info_options_page','',3);
  add_menu_page('スライダー設定', 'スライダー設定', 'administrator', 'answer_slider_menu', 'create_answer_slider_page','',4);
  add_action('admin_init', 'register_answer_para_settings');
}
add_action('admin_menu', 'answer_info_menu');

function register_answer_para_settings() {
  register_setting( 'answer-settings-group', 'answer_name' );
  register_setting( 'answer-settings-group', 'answer_post' );
  register_setting( 'answer-settings-group', 'answer_addr' );
  register_setting( 'answer-settings-group', 'answer_master' );
  register_setting( 'answer-settings-group', 'answer_tel' );
  register_setting( 'answer-settings-group', 'answer_fax' );
  register_setting( 'answer-settings-group', 'answer_mail' );

  register_setting( 'answer-slider-group', 'answer_slider_img_id_1' );
  register_setting( 'answer-slider-group', 'answer_slider_img_id_2' );
  register_setting( 'answer-slider-group', 'answer_slider_img_id_3' );
}


// カスタムメニューページを読み込む
function answer_info_options_page(){
  require TEMPLATEPATH.'/admin/info-input.php';
}
function create_answer_slider_page() {
  require TEMPLATEPATH.'/admin/top-slider.php';
}


//管理画面全てのページで独自のJavaScriptを読み込む
function my_admin_scripts() {
  wp_register_script(
      'mediauploader',
      get_stylesheet_directory_uri( ). '/js/mediauploader.js',
      array( 'jquery' ),
      false,
      true
  );

  //メディアライブラリ
  wp_enqueue_media();

  //作成した javascript
  wp_enqueue_script( 'mediauploader');
}
add_action( 'admin_print_scripts', 'my_admin_scripts' );




//投稿ページ等への投稿ページを追加するためのアクションフック
add_action('admin_menu', 'add_custom_inputbox');
//追加した表示項目のデータ更新・保存のためのアクションフック
add_action('save_post', 'save_custom_aero_postdata');
add_action('save_post', 'save_custom_accessory_postdata');

//入力項目がどの投稿タイプのページに表示されるのかの設定
function add_custom_inputbox() {
  add_meta_box('aero-info-id', 'エアロパーツ追加情報入力欄', 'custom_area_aero', 'aero-parts', 'normal', 'high');
  add_meta_box('accessory-info-id', 'アクセサリー追加情報入力欄', 'custom_area_accessory', 'cars-accessory', 'normal', 'high');
}

//管理画面に表示される内容
function custom_area_aero(){
  //これがないと入力欄が更新されない！
  global $post;

  //CSRF対策の設定（フォームにhiddenフィールドとして追加するためのnonceを「my_nonce」として設定）
  wp_nonce_field(wp_create_nonce(__FILE__), 'my_nonce');

  echo 'メーカー：<input type="text" name="car_maker" size="60" value="'.get_post_meta($post->ID,'car_maker',true).'"><br>';
  echo '車種1：<input type="text" name="car_type1" size="60" value="'.get_post_meta($post->ID,'car_type1',true).'"><br>';
  echo '<p>例:baccarat</p>';
  echo '車種2：<input type="text" name="car_type2" size="60" value="'.get_post_meta($post->ID,'car_type2',true).'"><br>';
  echo '型式：<input type="text" name="car_type_num" size="60" value="'.get_post_meta($post->ID,'car_type_num',true).'"><br>';
}
//投稿ボタンを押した際のデータ更新と保存
function save_custom_aero_postdata($post_id){
  global $post;

  //設定したnonce を取得（CSRF対策）
  $my_nonce = isset($_POST['my_nonce']) ? $_POST['my_nonce'] : null;
  //nonce を確認し、値が書き換えられていれば、何もしない（CSRF対策）
  if(!wp_verify_nonce($my_nonce, wp_create_nonce(__FILE__))) {
    return $post_id;
  }

  // データチェック
  if (array_key_exists('post_type', $_POST) && 'page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }

  //car_maker
  if(isset($_POST['car_maker'])){
    $car_maker = $_POST['car_maker'];
  }else{
    $car_maker = '';
  }
  //-1になると項目が変わったことになるので、項目を更新する
  if( strcmp($car_maker,get_post_meta($post_id, 'car_maker', true)) != 0 ){
    update_post_meta($post_id, 'car_maker',$car_maker);
  }elseif($car_maker == ""){
    delete_post_meta($post_id, 'car_maker',get_post_meta($post_id,'car_maker',true));
  }

  //car_type1
  if(isset($_POST['car_type1'])){
    $car_type1 = $_POST['car_type1'];
  }else{
    $car_type1 = '';
  }
  if( strcmp($car_type1,get_post_meta($post_id, 'car_type1', true)) != 0 ){
    update_post_meta($post_id, 'car_type1',$car_type1);
  }elseif($car_type1 == ""){
    delete_post_meta($post_id, 'car_type1',get_post_meta($post_id,'car_type1',true));
  }

  //car_type2
  if(isset($_POST['car_type2'])){
    $car_type2 = $_POST['car_type2'];
  }else{
    $car_type2 = '';
  }
  if( strcmp($car_type2,get_post_meta($post_id, 'car_type2', true)) != 0 ){
    update_post_meta($post_id, 'car_type2',$car_type2);
  }elseif($car_type2 == ""){
    delete_post_meta($post_id, 'car_type2',get_post_meta($post_id,'car_type2',true));
  }

  //car_type_num
  if(isset($_POST['car_type_num'])){
    $car_type_num = $_POST['car_type_num'];
  }else{
    $car_type_num = '';
  }
  if( strcmp($car_type_num,get_post_meta($post_id, 'car_type_num', true)) != 0 ){
    update_post_meta($post_id, 'car_type_num',$car_type_num);
  }elseif($car_type_num == ""){
    delete_post_meta($post_id, 'car_type_num',get_post_meta($post_id,'car_type_num',true));
  }
}

//管理画面に表示される内容
function custom_area_accessory(){
  //これがないと入力欄が更新されない！
  global $post;
  //CSRF対策の設定（フォームにhiddenフィールドとして追加するためのnonceを「my_nonce」として設定）
  wp_nonce_field(wp_create_nonce(__FILE__), 'my_nonce');

  echo 'ID：<input type="text" name="accessory_num" size="60" value="'.get_post_meta($post->ID,'accessory_num',true).'"><br>';
  echo '名前：<input type="text" name="accessory_name" size="60" value="'.get_post_meta($post->ID,'accessory_name',true).'"><br>';
  echo '価格：<input type="text" name="accessory_price" size="60" value="'.get_post_meta($post->ID,'accessory_price',true).'"><br>';
}
//投稿ボタンを押した際のデータ更新と保存
function save_custom_accessory_postdata($post_id){
  global $post;

  //設定したnonce を取得（CSRF対策）
  $my_nonce = isset($_POST['my_nonce']) ? $_POST['my_nonce'] : null;
  //nonce を確認し、値が書き換えられていれば、何もしない（CSRF対策）
  if(!wp_verify_nonce($my_nonce, wp_create_nonce(__FILE__))) {
    return $post_id;
  }

  // データチェック
  if (array_key_exists('post_type', $_POST) && 'page' == $_POST['post_type']) {
    if (!current_user_can('edit_page', $post_id)) {
      return $post_id;
    }
  } elseif (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }

  //accessory_num
  if(isset($_POST['accessory_num'])){
    $accessory_num = $_POST['accessory_num'];
  }else{
    $accessory_num = '';
  }
  //-1になると項目が変わったことになるので、項目を更新する
  if( strcmp($accessory_num,get_post_meta($post_id, 'accessory_num', true)) != 0 ){
    update_post_meta($post_id, 'accessory_num',$accessory_num);
  }elseif($accessory_num == ""){
    delete_post_meta($post_id, 'accessory_num',get_post_meta($post_id,'accessory_num',true));
  }
  //accessory_name
  if(isset($_POST['accessory_name'])){
    $accessory_name = $_POST['accessory_name'];
  }else{
    $accessory_name = '';
  }
  if( strcmp($accessory_name,get_post_meta($post_id, 'accessory_name', true)) != 0 ){
    update_post_meta($post_id, 'accessory_name',$accessory_name);
  }elseif($accessory_name == ""){
    delete_post_meta($post_id, 'accessory_name',get_post_meta($post_id,'accessory_name',true));
  }
  //accessory_price
  if(isset($_POST['accessory_price'])){
    $accessory_price = $_POST['accessory_price'];
  }else{
    $accessory_price = '';
  }
  if( strcmp($accessory_price,get_post_meta($post_id, 'accessory_price', true)) != 0 ){
    update_post_meta($post_id, 'accessory_price',$accessory_price);
  }elseif($accessory_price == ""){
    delete_post_meta($post_id, 'accessory_price',get_post_meta($post_id,'accessory_price',true));
  }
}


//CSS、JavaScriptを読み込む
function my_scripts() {
  wp_enqueue_style( 'sliderPro-css', get_template_directory_uri().'/css/slider-pro.min.css' );
  wp_enqueue_script( 'sliderPro-script', get_template_directory_uri().'/js/jquery.sliderPro.min.js' , array(), date('U'));
  wp_enqueue_script( 'parlx-script', get_template_directory_uri().'/js/parallax.js' , array(), date('U'));
  wp_enqueue_script( 'my-script', get_template_directory_uri().'/js/my.js' , array());
}
add_action( 'wp_enqueue_scripts', 'my_scripts' );


//2週間(14日)以内ならNEWを表示
$TODAY = strtotime(date('Y-m-d'));
function check_new_post($date) {
  global $TODAY;
  $date = strtotime($date);
  $dayDiff = abs($TODAY - $date) / 86400; //(60 * 60 * 24)
  return ($dayDiff < 14);
}

//タイトルの長さ（文字数）を変更
function the_short_title($length = 20) {
  $ret = get_the_title( $post->ID );
  if ( mb_strlen($ret) > $length ) {
    $ret = mb_substr( $ret, 0, $length ) . '...';
  }
  return $ret;
}

//抜粋の長さ（文字数）を変更
function new_excerpt_mblength($length) {
  return 20;
}
add_filter('excerpt_mblength', 'new_excerpt_mblength');


?>
