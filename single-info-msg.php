<?php get_header(); ?>
<div class="content"><!-- Contents -->

  <section class="post-news">

    <div class="post-news-container">
      <h2>News</h2>
      <h3><?php the_title(); ?></h3>

      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>

          <div class="post-news-day">
            <div><?php the_time('Y年m月d日') ?></div>
          </div>

          <div class="post-news-box clearfix">

            <?php if(check_new_post( get_post_time('Y-m-d') )): ?>
              <div class="post-news-thum pickup-img">
            <?php else: ?>
              <div class="post-news-thum">
            <?php endif ?>

            <?php if(has_post_thumbnail()): ?>
              <?php the_post_thumbnail(array( 200, 170 )); ?>
            <?php else: ?>
              <img src="<?php bloginfo('template_directory'); ?>/img/logo_thum.png" width="200" height="170" alt="noimage"/>
            <?php endif ?>
            </div>

            <p><?php the_content(); ?></p>



            <div class="post-news-cat">
              <?php echo get_the_term_list($post->ID, 'info-msg-cat','',' '); ?>
            </div>


          </div>

        <?php endwhile; ?>

      <?php else : ?>
        <h3>ページが見つかりませんでした。</h3>
      <?php endif; ?>

    </div>
  </section>
</div><!-- content -->

<?php get_footer(); ?>