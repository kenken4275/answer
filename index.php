<?php get_header(); ?>
<!-- Contents -->
<section>
  <div class="content">
    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
        <div class="post">
          <h1><?php the_title(); ?></h1>
          <?php if(has_post_thumbnail()) { echo the_post_thumbnail(); } ?>
          <?php the_content('続きを読む'); ?>
        </div><!-- /.post -->
      <?php endwhile; ?>
      <div class="nav-below">
        <span class="nav-previous"><?php next_posts_link('古い記事へ') ?></span>
        <span class="nav-next"><?php previous_posts_link('新しい記事へ') ?></span>
      </div><!-- /.nav-below -->
    <?php else : ?>
      <h2 class="title">ページが見つかりませんでした。</h2>
      <p>検索で見つかるかもしれません。</p><br />
      <?php get_search_form(); ?>
    <?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>
