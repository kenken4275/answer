<div class="slider-container">
  <?php if ( wp_is_mobile() ) : ?>
    <p><img src="<?php bloginfo('template_url'); ?>/img/logo_mini.png"/></p>
  <?php else: ?>
    <p><img src="<?php bloginfo('template_url'); ?>/img/logo.png"/></p>
  <?php endif; ?>

  <div class="slider-pro" id="slider1">
    <div class="sp-slides">
      <!-- Slide 1 -->
      <div class="sp-slide">
        <?php if ( wp_is_mobile() ) : ?>
          <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_1') , 'medium'); ?>
        <?php else: ?>
          <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_1') , 'large'); ?>
        <?php endif; ?>
        <?php $img_url = get_stylesheet_directory_uri() . "/img/pic1mini.jpg"; ?>
        <?php if( $img_src) : ?>
        <?php $img_url = $img_src[0]; ?>
        <?php endif; ?>
        <img class="sp-image" src="<?php echo $img_url; ?>"/>
      </div>

      <!-- Slide 2 -->
      <div class="sp-slide">
        <?php if ( wp_is_mobile() ) : ?>
          <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_2') , 'medium'); ?>
        <?php else: ?>
          <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_2') , 'large'); ?>
        <?php endif; ?>
        <?php $img_url = get_stylesheet_directory_uri() . "/img/pic2mini.jpg"; ?>
        <?php if( $img_src) : ?>
        <?php $img_url = $img_src[0]; ?>
        <?php endif; ?>
        <img class="sp-image" src="<?php echo $img_url; ?>"/>
      </div>

      <!-- Slide 3 -->
      <div class="sp-slide">
        <?php if ( wp_is_mobile() ) : ?>
          <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_3') , 'medium'); ?>
        <?php else: ?>
          <?php $img_src = wp_get_attachment_image_src( get_option('answer_slider_img_id_3') , 'large'); ?>
        <?php endif; ?>
        <?php $img_url = get_stylesheet_directory_uri() . "/img/pic3mini.jpg"; ?>
        <?php if( $img_src) : ?>
        <?php $img_url = $img_src[0]; ?>
        <?php endif; ?>
        <img class="sp-image" src="<?php echo $img_url; ?>"/>
      </div>

    </div>
  </div>
</div>