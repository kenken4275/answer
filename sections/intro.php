<div class="intro-container">
  <h2>Welcome</h2>
  <p>ようこそ！オートガレージAnswerのホームページへ。</p>
  <p>Answerでは豊富な取り揃えのエアロパーツ、アクセサリーに、
  <p>中古車の販売を行っております。</p>
  <p>宜しくお願い致します。</p>

  <div class="intro-wrapper">
    <div class="intro-box">
      <div class="intro-img">
        <img src="<?php bloginfo('template_url'); ?>/img/intro1.jpg" width="200" height="200" />
      </div>
      <div class="intro-msg">全国納車実績多数!!<br>確かな技術と豊富な経験で<br>安心な中古車販売を実現しています</div>
    </div>
    <div class="intro-box">
      <div class="intro-img">
        <img src="<?php bloginfo('template_url'); ?>/img/intro2.jpg" width="200" height="200" />
      </div>
      <div class="intro-msg">オリジナルエアロパーツ!!<br>アクセサリーも取り扱っています</div>
    </div>
    <div class="intro-box">
      <div class="intro-img">
        <img src="<?php bloginfo('template_url'); ?>/img/intro3.jpg" width="200" height="200" />
      </div>
      <div class="intro-msg">大切なお車のドレスアップも<br>承っております</div>
    </div>
  </div>
</div>
