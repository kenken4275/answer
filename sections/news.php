<?php
  $args = array(
      'post_type' => array('info-msg'),
      'posts_per_page' => 8, //取得数は8に制限
      'orderby' => 'date', //日付でソート
      'order' => 'DESC'    //日付が新しい順（降順）で表示
  );
  $the_query = new WP_Query($args);
?>

<div class="parallax-window-news" data-parallax="scroll" data-speed="0.6" data-position="center center" data-image-src="<?php echo get_template_directory_uri(); ?>/img/parallax01.jpg" data-natural-width="1000" data-natural-height="1500">
  <div class="infomation_wrapper">

    <h2  id="h2_news">News</h2>

    <?php if($the_query->have_posts()): ?>
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <div class="infomation_box">

          <div class="infomation_box_time">
            <p><?php the_time('Y年n月j日'); ?></p>
          </div>

          <?php if(check_new_post( get_post_time('Y-m-d') )): ?>
            <div class="infomation_box_thum pickup-img">
          <?php else: ?>
            <div class="infomation_box_thum">
          <?php endif ?>

          <?php echo '<a href="'. get_permalink() .'">'; ?>
            <?php if( has_post_thumbnail()): ?>
              <?php the_post_thumbnail( array( 200, 170 )); ?>
            <?php else: ?>
              <img src="<?php bloginfo('template_url'); ?>/img/logo_thum.png" width="200" height="170" alt="no image"/>
            <?php endif; ?>
          </a>

          </div>

          <div class="infomation_box_title">
            <?php echo '<a href="'. get_permalink() .'">'. the_short_title(40) . '</a>'; ?>
          </div>

          <div class="infomation_box_content">
            <!--<?php the_excerpt(); ?>-->
          </div>

          <div class="infomation_box_cat">
            <?php echo get_the_term_list($post->ID, 'info-msg-cat','',' '); ?>
          </div>

        </div>

      <?php endwhile; ?>
    <?php endif ?>

    <div id="go-to-newslist-box">
      <a href="<?php echo get_permalink(8); ?>" id="go-to-newslist"><i class="fas fa-angle-double-right"></i>More</a>
    </div>
  </div>

</div>
