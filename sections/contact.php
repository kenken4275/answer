<div class="parallax-window-contact" data-parallax="scroll" data-speed="0.6" data-position="center center" data-image-src="<?php echo get_template_directory_uri(); ?>/img/parallax02.jpg" data-natural-width="1000" data-natural-height="666">
  <div class="contact_wrapper">
    <h2 id="h2_contact">Contact</h2>
    <?php echo do_shortcode('[contact-form-7 id="19" title="コンタクトフォーム 1"]'); ?>
  </div>
</div>
