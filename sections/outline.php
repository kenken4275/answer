<div class="outline-container">
<div class="outline_wrapper">
  <h2>Outline</h2>

  <div class="content_in_left">
    <p><?php echo get_option( 'answer_name' ); ?>はインターネットを用いた通信販売において、健全な電子取引の普及に寄与し、お客様にご信頼いただくために、通信販売の法規（訪問販売法）及びその他の関係法令に基づき、ガイドラインに沿って必要事項を表示しています。</p>
    <table class="gaiyo">
      <tbody>
        <tr>
          <td class="gaiyo1">販売業者</td>
          <td class="gaiyo2"><?php echo get_option( 'answer_name' ); ?></td>
        </tr>
        <tr>
          <td class="gaiyo1">郵便番号</td>
          <td class="gaiyo2"><?php echo get_option( 'answer_post' ); ?></td>
        </tr>
        <tr>
          <td class="gaiyo1">所在地</td>
          <td class="gaiyo2"><?php echo get_option( 'answer_addr' ); ?></td>
        </tr>
        <tr>
          <td class="gaiyo1">代表者</td>
          <td class="gaiyo2"><?php echo get_option( 'answer_master' ); ?></td>
        </tr>
        <tr>
          <td class="gaiyo1">TEL</td>
          <td class="gaiyo2"><?php echo get_option( 'answer_tel' ); ?></td>
        </tr>
        <tr>
          <td class="gaiyo1">FAX</td>
          <td class="gaiyo2"><?php echo get_option( 'answer_fax' ); ?></td>
        </tr>
        <tr>
          <td class="gaiyo1">E-Mail</td>
          <td class="gaiyo2"><?php echo get_option( 'answer_mail' ); ?></td>
        </tr>
      </tbody>
    </table>

  </div>

  <div class="content_in_right">

    <img src="<?php bloginfo('template_url'); ?>/img/answer01.jpg" id="outline-img-01" alt="Answerにようこそ" />
    <div class="memo-top">
      <div class="memo-title">販売価格：</div>
      <div class="memo-memo">各商品毎に表示</div>
    </div>
    <div class="memo-top">
      <div class="memo-title">お支払方法：</div>
      <div class="memo-memo">銀行振込・代金引換・現金書留<br>（ご入金確認後発送致します。在庫切れの場合は改めてご連絡差し上げます。）</div>
    </div>
    <div class="memo-top">
      <div class="memo-title">商品以外の必要料金：</div>
      <div class="memo-memo">送料（代金引換の場合は代金引換手数料）</div>
    </div>
    <div class="memo-top">
      <div class="memo-title">商品引渡し時期：</div>
      <div class="memo-memo">通常、ご注文受付から２日～２週間</div>
    </div>
    <div class="memo-top">
      <div class="memo-title">不良品：</div>
      <div class="memo-memo">商品到着から７日以内にご連絡<br>弊社指定場所に着払いで返品後、良品とご交換</div>
    </div>
    <div class="memo-top">
      <div class="memo-title">返品期限：</div>
      <div class="memo-memo">商品到着から７日以内にご連絡</div>
    </div>
    <div class="memo-top">
      <div class="memo-title">返品送料：</div>
      <div class="memo-memo">不具合の場合、弊社負担<br>お客様都合による交換・返品の場合、お客様ご負担</div>
    </div>
    <img src="<?php bloginfo('template_url'); ?>/img/sticker.png" width="182" height="178" alt="G-ANSWER" />

  </div>

</div>
</div>
