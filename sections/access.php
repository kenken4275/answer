<div class="access-container">
  <div class="access_wrapper">
    <h2>Access</h2>
    <p>小倉東ＩＣ　もしくは、長野ＩＣ　下車後、苅田-行橋方面へ</p>
    <p>ＪＲ日豊本線、下曽根駅より、徒歩１５分</p>

    <div class="google-maps">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3314.293422482911!2d130.922576950587!3d33.83054033673366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3543c05bb43246b7%3A0x806f5127bb51c4c7!2z44CSODAwLTAyMjcg56aP5bKh55yM5YyX5Lmd5bee5biC5bCP5YCJ5Y2X5Yy65rSl55Sw5paw55S677yU5LiB55uu77yR4oiS77yT!5e0!3m2!1sja!2sjp!4v1492476527694" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
</div>

