      <!-- Footer -->			
      <div id="footer">

        <?php dynamic_sidebar( 'footer-widget' ); ?>

        <div id="footer-menu">
          <?php wp_nav_menu( array ( 'theme_location' => 'footer-navbar' ) ); ?>
        </div>

        <div class="copy">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All rights reserved.</div>

      </div><!-- /#footer -->

    </div><!-- /#wrapper -->
  </div><!-- /#page -->

  <?php wp_footer(); ?>
</body>
</html>